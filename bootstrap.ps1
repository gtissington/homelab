set-executionpolicy unrestricted -force
. { iwr -useb https://boxstarter.org/bootstrapper.ps1 } | iex
get-boxstarter -Force

$cred = Get-Credential -message "Please enter password for GTISS" -user "gtiss"
# $secpasswd = ConvertTo-SecureString "q" -AsPlainText -Force
# $cred = New-Object System.Management.Automation.PSCredential ("admin", $secpasswd)
Install-BoxstarterPackage -PackageName "https://gitlab.com/gtissington/homelab/-/raw/master/base-box.ps1" -Credential $cred
