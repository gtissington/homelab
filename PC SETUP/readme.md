Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine

#Install chocolatey
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

choco install microsoft-windows-terminal
choco install vscode
choco install git
choco install starship